/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.util

import android.content.SharedPreferences

object Config {
    const val regionalBeerTypeFilter = "Config1"
    private const val userDataGoogle = "Config2"
    private const val modifyDataGoogle = "Config3"

    lateinit var prefs : SharedPreferences

    var userSheetAccess : Boolean
        set(value) { prefs.edit().putBoolean( userDataGoogle, value ).apply() }
        // I have no clue anymore why it is like this... seems wrong
        get() { return if( modifyAccess ) modifyAccess else prefs.getBoolean( userDataGoogle, false ) }

    var modifyAccess : Boolean
        set(value) { prefs.edit().putBoolean( modifyDataGoogle, value ).apply() }
        get() { return prefs.getBoolean( modifyDataGoogle, false ) }

}