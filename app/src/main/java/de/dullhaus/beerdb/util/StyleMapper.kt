/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.util

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.preference.PreferenceManager
import com.google.common.collect.HashBiMap
import de.dullhaus.beerdb.Data
import de.dullhaus.beerdb.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

object StyleMapper {
    // All of them (used for spinner)
    var allStylesList = listOf<String>()
    // Regional subset ( used for spinner)
    var regionalStylesList = mutableListOf<String>()
    // Used to derive the string to display
    private var viewStylesMap = mutableMapOf<Int, String>()

    private var rdy = false

    fun regionalStylesAvail() = regionalStylesList.isNotEmpty()

    fun getIdFromName( name : String ) : Int {
        return allStylesBiMap.inverse()[name] ?: 0
    }

    fun getDisplayName( id : Int ) = if( rdy ) viewStylesMap[id] else Data.beerStr
    fun getDetailedName( id : Int ) = if( rdy ) allStylesBiMap[id] else Data.beerStr

    lateinit var prefs : SharedPreferences

    fun getSpinnerId( id : Int ) : Int {
        val name = allStylesBiMap[id]
        val useRegional = prefs.getBoolean( Config.regionalBeerTypeFilter, true)
        val idx = if(useRegional) regionalStylesList.indexOf( name ) else allStylesList.indexOf( name )
        return if( idx >= 0 ) idx else 0
    }

    private var allStylesBiMap = HashBiMap.create<Int, String>()
    const val TAG = "StyleMapper"
    private val scope = CoroutineScope(Dispatchers.IO + Job() )
    fun reflect( context : Context ){
        scope.launch {
            prefs = PreferenceManager.getDefaultSharedPreferences( context )
            val d = R.string::class
            for(elmt in d.members) {
                if(elmt.name.startsWith("type_")) {
                    val oha = elmt.call()
                    if(oha is Int) {
                        var str = context.getString(oha)
                        val splitted = elmt.name.split("_")
                        try {
                            val nr = splitted.last().toInt()
                            val isRegional = str.startsWith("+")
                            if(isRegional) str = str.removePrefix("+")

                            if(str.contains("_")) {
                                val names = str.split("_")
                                viewStylesMap[nr] = names[0]
                                if(isRegional) regionalStylesList.add(names[1])
                                allStylesBiMap[nr] = names[1]
                            }
                            else {
                                viewStylesMap[nr] = str
                                allStylesBiMap[nr] = str
                                if(isRegional) regionalStylesList.add(str)
                            }

                        }
                        catch(ex : NumberFormatException) {
                            Log.e(TAG, "couldn't derive id from ${elmt.name}!")
                            continue
                        }
                    }
                }
            }
            regionalStylesList.sort()
            allStylesList = allStylesBiMap.values.toList().sorted()
            rdy = true
        }
    }
}
