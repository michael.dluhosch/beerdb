/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.util

import android.view.View
import android.widget.EditText
import androidx.fragment.app.FragmentManager
import de.dullhaus.beerdb.R
import de.dullhaus.beerdb.ui.main.BeerViewModel
import java.time.LocalDate

object DatePickers {

    fun show( v: View, fm : FragmentManager, beerVM : BeerViewModel ) {
        val beer = beerVM.beerLiveD.value!!

        when( v.id ) {
            R.id.modify_brewed_since -> {
                val beerDbData = beer.beerDbData
                if(beerDbData.brewedSince == null) beerDbData.brewedSince = LocalDate.now()
                val txt = v.findViewById<EditText>(R.id.modify_brewed_since)
                val newFragment = DatePickerFragment( txt, beer.beerDbData::brewedSince )
                newFragment.show( fm, "datePickerSince")
            }
            R.id.modify_brewed_until -> {
                val beerDbData = beer.beerDbData
                if( beerDbData.brewedUntil == null ) beerDbData.brewedUntil = LocalDate.now()
                val txt = v.findViewById<EditText>(R.id.modify_brewed_until)
                val newFragment = DatePickerFragment( txt, beer.beerDbData::brewedUntil )
                newFragment.show( fm, "datePickerUntil")
            }
        }
    }
}
