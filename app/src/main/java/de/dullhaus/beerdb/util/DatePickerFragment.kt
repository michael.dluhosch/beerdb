/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.util

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import kotlin.reflect.KMutableProperty0

// To get pass by reference you need reflection... wtf
// Or why the F a LocalDate is immutable????
class DatePickerFragment(val etxt : EditText, var date : KMutableProperty0<LocalDate?>) : DialogFragment(), DatePickerDialog.OnDateSetListener {

    override fun onCreateDialog(savedInstanceState : Bundle?) : Dialog {
            return DatePickerDialog( requireActivity(), this, date.get()!!.year, date.get()!!.monthValue - 1 , date.get()!!.dayOfMonth )
    }

    override fun onDateSet(view : DatePicker, year : Int, month : Int, day : Int) {
        // this is what pass by reference looks like in java/kotlin
        date.set( LocalDate.of( year, month + 1 , day) )

        etxt.setText(date.get()!!.format( DateTimeFormatter.ofLocalizedDate( FormatStyle.MEDIUM ) ) )
    }
}
