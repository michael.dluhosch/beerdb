/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *       http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.util

import android.widget.ImageView
import com.google.api.services.drive.DriveScopes
import com.google.api.services.sheets.v4.SheetsScopes
import de.dullhaus.beerdb.Beer
import de.dullhaus.beerdb.BeerPicCache
import de.dullhaus.beerdb.Data
import kotlinx.coroutines.*
import java.time.LocalDate
import java.time.ZonedDateTime
import java.time.format.DateTimeParseException
import java.util.*

object Utility {

    fun convStrToZonedDateTime(str : String) : ZonedDateTime {
        return try {
            val date = ZonedDateTime.parse(str)
            date
        }
        catch(ex : DateTimeParseException) {
            ZonedDateTime.now()
        }
    }

    fun convStrToBrewDate(str : String) : LocalDate? {
        return try {
            val date = LocalDate.parse(str)
            date
        }
        catch(ex : DateTimeParseException) {
            null
        }
    }

    fun doubleToStr(dbl : Double) : String {
        return android.icu.text.NumberFormat.getInstance( Locale.US ).format( dbl )
    }

    fun strToDouble(str : String) : Double {
        return try {
            android.icu.text.NumberFormat.getInstance( Locale.US ).parse(str).toDouble()
        }
        catch( ex : java.text.ParseException ) {
            -1.0
        }
    }

    val scopes : List<String>
        get() {
            return if(Config.userSheetAccess && !Config.modifyAccess) {
                listOf(
                    DriveScopes.DRIVE_FILE
                )
            }
            else {
                listOf(
                    SheetsScopes.SPREADSHEETS,
                    DriveScopes.DRIVE_FILE
                )
            }
        }

    private val scope = CoroutineScope( Dispatchers.IO + Job() )

    fun loadPic( b : Beer, v : ImageView ) {
        scope.launch {
            // TODO Make image blurry or other effect to show we are loading
            val elBitmap = BeerPicCache.getPic(b.beerDbData.picUrl)

            withContext( Dispatchers.Main ) {
                if(elBitmap != null) {
                    v.setImageBitmap(elBitmap)
                }
                else {
                    v.setImageDrawable(Data.noPic)
                }
            }
        }
    }


}