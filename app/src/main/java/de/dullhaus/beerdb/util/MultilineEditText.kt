/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.util

import android.app.Activity
import android.content.Context
import android.text.InputType
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

// This is from https://gist.github.com/Dogesmith/2b98df97b4fca849ff94

fun prepEditText( et : EditText, activity : Activity ? ) {
    et.setRawInputType(InputType.TYPE_CLASS_TEXT)
    et.setImeActionLabel("WTF", EditorInfo.IME_ACTION_DONE )
    et.imeOptions = EditorInfo.IME_ACTION_DONE

    et.setOnEditorActionListener { v, actionId, event ->
        if( event == null ) {
            when(actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    // Capture soft enters in a singleLine EditText that is the last EditText
                    // This one is useful for the new list case, when there are no existing ListItems
                    et.clearFocus()
                    val inputMethodManager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputMethodManager.hideSoftInputFromWindow( v.windowToken, InputMethodManager.RESULT_UNCHANGED_SHOWN )
                }
                EditorInfo.IME_ACTION_NEXT -> {
                    // Capture soft enters in other singleLine EditTexts
                }
                EditorInfo.IME_ACTION_GO -> {
                }
                else -> {
                    // Let the system handle all other null KeyEvents
                    return@setOnEditorActionListener false
                }
            }
        } else if( actionId == EditorInfo.IME_NULL ) {
            // Capture most soft enters in multi-line EditTexts and all hard enters;
            // They supply a zero actionId and a valid keyEvent rather than
            // a non-zero actionId and a null event like the previous cases.
            if ( event.action == KeyEvent.ACTION_DOWN ) {
                // We capture the event when the key is first pressed.
            } else {
                // We consume the event when the key is released.
                return@setOnEditorActionListener true
            }
        } else {
            // We let the system handle it when the listener is triggered by something that
            // wasn't an enter.
            return@setOnEditorActionListener false
        }
        return@setOnEditorActionListener true
    }
}