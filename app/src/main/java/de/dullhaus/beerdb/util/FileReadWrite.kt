/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.util

import android.util.Log
import java.io.*

inline fun <reified T> readFromFile( file : File ) : T ? {
    val TAG = "FileReadWrite"
    if( !file.exists() ) return null

    val objectInputStream = ObjectInputStream( FileInputStream( file ) )
    try {

        val obj = objectInputStream.readObject()
        objectInputStream.close()
        if( obj is T )
            return obj
    }
    catch ( e : FileNotFoundException)
    {
        Log.w( TAG, "File not Found Exception: " + e.message )
    }
    catch( e : IOException) {
        Log.w( TAG, "IO Exception: " + e.message )
    }
    catch ( e : ClassNotFoundException ) {
        Log.w( TAG, "Class Not Found Exception: " + e.message )
    }
    finally {
        objectInputStream.close()
    }

    return null
}

fun <T> writeToFile( file : File, data : T ) {
    val objectOutputStream = ObjectOutputStream( FileOutputStream( file ) )
    objectOutputStream.writeObject( data )
    objectOutputStream.close()
}