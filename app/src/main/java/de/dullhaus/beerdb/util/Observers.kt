/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.util

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2
import de.dullhaus.beerdb.Beer
import de.dullhaus.beerdb.Data
import de.dullhaus.beerdb.Kraken

object Observers {

    private var lastSize = 0
    private var lastViewPager : ViewPager2 ? = null

    fun register( lifecycleOwner : LifecycleOwner ) {
        // This adds or removes the third tab
        val thirdTabChecker = Observer<MutableList<Beer>> { liveBeers ->
            val removed = lastSize != 0 && liveBeers.size == 0
            val added =  lastSize == 0 && liveBeers.size != 0
            if( removed || added  || lastViewPager != Kraken.viewPager ) {
                Data.updateThirdTab()
            }
            lastSize = liveBeers.size
            lastViewPager = Kraken.viewPager
        }

        val selectBeerDbTab = Observer<MutableList<Beer>> { liveBeers ->
            if( liveBeers.none { it.userData != null } ) {
                Kraken.viewPager.currentItem = 1
            }
        }

        val countryPrefs = Observer<MutableList<Beer>> { liveBeers ->
            // get all countries
            val countries = liveBeers.distinctBy { it.beerDbData.countryCode }
            // Count occurrences per country
            val countryOccurrences = countries.map { countryBeer ->
                Pair( countryBeer.beerDbData.countryCode, liveBeers.filter { it.beerDbData.countryCode == countryBeer.beerDbData.countryCode  }.size ) }.toMutableList()
            countryOccurrences.sortByDescending { it.second }
            // Take 10 most common countries
            Data.countryPref = countryOccurrences.subList(0,9).joinToString(separator = ",") { it.first }
        }

        Data.modRequestsLive.observe( lifecycleOwner, thirdTabChecker )
        Data.beerDbLive.observeForever( Data.syncDiskCacheObsi )
        Data.beerDbLive.observe( lifecycleOwner, selectBeerDbTab )
        Data.beerDbLive.observe( lifecycleOwner, countryPrefs )
    }
}