/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.preference.PreferenceManager
import de.dullhaus.beerdb.util.Config
import de.dullhaus.beerdb.util.Observers
import de.dullhaus.beerdb.util.Utility

class MainActivity : AppCompatActivity() {

    private  val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Kraken.main = this

        setContentView( R.layout.activity_main )

        Observers.register( this )

        Config.prefs = PreferenceManager.getDefaultSharedPreferences( this )

        Kraken.authMan = AuthenticationManager(
            lazyOf( this ),
            Utility.scopes
        )
    }

    override fun onStart() {
        // Hopefully not necessary
        super.onStart()
        // TODO: think about what should happen when there is no inet
//        val lol = NetwokMonitor.inetAvail
        Kraken.authMan.startLogin()
    }
    

    override fun onDestroy() {
        super.onDestroy()
        BeerPicCache.close()
    }

//    override fun onBackPressed() {
//        if( Kraken.sectionsPagerAdapter.detailsActive() )
//            Kraken.sectionsPagerAdapter.closeDetail()
//        else if( Kraken.sectionsPagerAdapter.addBeerActive() )
//            Kraken.sectionsPagerAdapter.closeNewBeer()
//        else if( Kraken.sectionsPagerAdapter.adminBeerActive() )
//            Kraken.sectionsPagerAdapter.closeAdminBeer()
//        else
//            super.onBackPressed()
//    }


    // This will be called from authMan as a result of startLogin
    fun loggedIn() {
//        if( Kraken.goToNewBeer ) {
//            Kraken.goToNewBeer = false
//            Kraken.sectionsPagerAdapter.newBeerClicked()
//        }
//        else if( Kraken.goToModify )
//        {
//            if( Kraken.sectionsPagerAdapter.detailsActive() ) {
//                Kraken.goToModify = false
////                Kraken.sectionsPagerAdapter.getCurrentDetails()?.enterModMode()
//            }
//            else {
//                Kraken.viewPager.currentItem = Kraken.goToViewPager
//                Kraken.beerToUserify?.let { Kraken.sectionsPagerAdapter.onItemClicked(it) }
//            }
//            Kraken.beerToUserify = null
//        }
//        else if( Kraken.beerToUserify != null && Kraken.beerToUserify?.userData == null ) {
//            Data.addBeerToUserBeers( Kraken.beerToUserify!! )
//            Kraken.beerToUserify = null
//        }

    }

    // This will be called from authMan as a result of startLogin
    fun notLoggedIn() {
//        val intent = Intent(this, SignInFragment::class.java)
//        startActivity( intent )
//        navGraphDir
        val vi = findViewById<View>(R.id.view_pager)
        findNavController( vi.id ).navigate(R.id.signInFragment)
    }
}