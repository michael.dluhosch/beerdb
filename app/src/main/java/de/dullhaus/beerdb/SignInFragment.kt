/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import de.dullhaus.beerdb.databinding.LoginScreenBinding

class SignInFragment : Fragment() {

    private var _binding: LoginScreenBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle?) : View {
        _binding = LoginScreenBinding.inflate( inflater, container, false )
        return binding.root
    }

    override fun onViewCreated(view : View, savedInstanceState : Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.signInButton.setSize( SignInButton.SIZE_WIDE )
        binding.loginProgressBar.visibility = View.INVISIBLE
        binding.signInButton.setOnClickListener {
            googleSignInLauncher.launch( Kraken.authMan.googleSignInClient.signInIntent )
        }
    }

    private val googleSignInLauncher = registerForActivityResult( ActivityResultContracts.StartActivityForResult()) { result : ActivityResult ->
        val task = GoogleSignIn.getSignedInAccountFromIntent( result.data )

        try {
            val account = task.getResult(ApiException::class.java)
            Kraken.authMan.setUpGoogleAccountCredential( account )
            if( Kraken.authMan.loggedInMtx.isLocked ) Kraken.authMan.loggedInMtx.unlock()
            else Log.w(TAG, "Logged in Mutex was not locked!")
            findNavController().popBackStack()
        }
        catch( e : ApiException )
        {
            Log.w(TAG, "something failed ${e.message} (${e.statusCode})")
            return@registerForActivityResult
        }
    }

    private val TAG = "SignInActivity"
}
