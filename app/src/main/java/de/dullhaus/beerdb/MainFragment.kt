/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import de.dullhaus.beerdb.databinding.FragmentMainBinding
import de.dullhaus.beerdb.ui.main.SectionsPagerAdapter
import de.dullhaus.beerdb.ui.main.dialogs.AreYouSure
import de.dullhaus.beerdb.ui.main.dialogs.Terms
import kotlinx.coroutines.*

class MainFragment : Fragment() {
    private var _binding: FragmentMainBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val TAG = "MainFragment"

    private val scopeUi = MainScope() + CoroutineName("MyActivity")

    override fun onCreateView(
        inflater : LayoutInflater, container : ViewGroup?,
        savedInstanceState : Bundle?
    ) : View {
        _binding = FragmentMainBinding.inflate( inflater, container, false )

        // Release another Kraken ;)
        Kraken.viewPager = binding.viewPager
        Kraken.sectionsPagerAdapter = SectionsPagerAdapter( requireActivity() )
        Kraken.viewPager.adapter = Kraken.sectionsPagerAdapter

        return binding.root
    }

    private val scope = CoroutineScope( Dispatchers.Default + Job() )
    override fun onViewCreated(view : View, savedInstanceState : Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val tabs: TabLayout = binding.tabs
        TabLayoutMediator(tabs, Kraken.viewPager) { tab, position ->
            tab.text = resources.getString( Data.TAB_TITLES[position] )
        }.attach()

        // Stuff which might take some time until it is created
        Kraken.initJob = scope.launch( Dispatchers.IO ) {
            Data.init( Kraken.main )

            Kraken.topAppBar = binding.topAppBar
            val searchItem = Kraken.topAppBar.menu.findItem( R.id.search )
            val removeItem = Kraken.topAppBar.menu.findItem( R.id.menu_remove )

            Kraken.topAppBar.setOnMenuItemClickListener {
                when( it.itemId ) {
                    R.id.search -> {
                        Log.i(TAG,"Rofl die Katy!")
                        true
                    }
                    R.id.menu_refresh -> {
                        val swipeLayout = Kraken.main.findViewById<SwipeRefreshLayout>(R.id.swipeContainer)
                        swipeLayout?.isRefreshing = true
                        scopeUi.launch {
                            try {
                                Data.handleRefresh( Kraken.main )
                            }
                            finally {
                                swipeLayout?.isRefreshing = false
                            }
                        }
                        true
                    }
                    R.id.menu_remove -> {
                        if( Kraken.viewPager.currentItem == 0 ) {
                            AreYouSure().show( Kraken.main.supportFragmentManager, "hellau" )
                        }
                        true
                    }
                    R.id.menu_terms -> {
                        Terms().show( Kraken.main.supportFragmentManager, "seizef" )
                        true
                    }
                    else -> false
                }
            }

            val searchView = searchItem.actionView as androidx.appcompat.widget.SearchView

//            searchView.setOnQueryTextListener(
//                object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
//                    override fun onQueryTextSubmit(query : String?) : Boolean {
//                        return false
//                    }
//
//                    override fun onQueryTextChange(newText : String?) : Boolean {
//                        if( Kraken.viewPager.currentItem == 1  ) Kraken.sectionsPagerAdapter.getBeerDbFragment()?.rcycAdapt?.doFiltering( newText )
////                        else if ( Kraken.viewPager.currentItem == 0  ) Kraken.sectionsPagerAdapter.getMyBeerFragment()?.rcycAdapt?.doFiltering( newText )
//                        else return true
//                        return false
//                    }
//                }
//            )

            Kraken.viewPager.registerOnPageChangeCallback( object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position : Int) {
                    super.onPageSelected(position)
//                    if( position == 0 ) {
//                        Kraken.sectionsPagerAdapter.getMyBeerFragment()?.rcycAdapt?.doFiltering( searchView.query.toString() )
//                    }
                    if( position == 1 ) {
                        Kraken.sectionsPagerAdapter.getBeerDbFragment()?.rcycAdapt?.doFiltering( searchView.query.toString() )
                        removeItem.isVisible = false
                    }
                }
            } )

            BeerPicCache.init( Kraken.main.cacheDir )

            NetwokMonitor.cm = Kraken.main.getSystemService(Context.CONNECTIVITY_SERVICE)  as ConnectivityManager
        }
    }
}
