/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import android.content.Context
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.Scope
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.util.ExponentialBackOff
import de.dullhaus.beerdb.util.Config
import kotlinx.coroutines.sync.Mutex

class AuthenticationManager(private val context: Lazy<Context>,
                             SCOPES : List<String> ) {

    init { applyScopes( SCOPES ) }

    lateinit var signInOptions : GoogleSignInOptions
    lateinit var googleSignInClient : GoogleSignInClient
    lateinit var googleAccountCredential : GoogleAccountCredential

    fun applyScopes( SCOPES : List<String> ) {
        // The E-Mail is important even if we don't use it. Without it you cannot save the account credentials for later use!
        signInOptions = GoogleSignInOptions.Builder( GoogleSignInOptions.DEFAULT_SIGN_IN )
            .requestScopes( Scope( SCOPES[0] ), *SCOPES.slice(1 until SCOPES.size).map{ Scope(it) }.toTypedArray() )
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient( context.value, signInOptions )

        googleAccountCredential = GoogleAccountCredential
            .usingOAuth2( context.value, SCOPES )
            .setBackOff( ExponentialBackOff() )
    }

    fun setUpGoogleAccountCredential( account : GoogleSignInAccount? ) {
        signInAccount = account
        googleAccountCredential.selectedAccount = account?.account
    }

    val loggedInMtx = Mutex(true)

    fun startLogin() {
        if(!loggedInMtx.isLocked) loggedInMtx.tryLock()

        if( !Config.userSheetAccess && !Config.modifyAccess ) {
            // So far user did not want to sign in at all
            loggedInMtx.unlock()
            (context.value as MainActivity).loggedIn()
            return 
        }

        val task  = googleSignInClient.silentSignIn()
        if( task.isSuccessful ) {
            setUpGoogleAccountCredential( task.result )
            loggedInMtx.unlock()
            (context.value as MainActivity).loggedIn()
        }
        else {
            task.addOnCompleteListener { taskret  ->
                if( taskret.isSuccessful ){
                    setUpGoogleAccountCredential( taskret.result )
                    loggedInMtx.unlock()
                    (context.value as MainActivity).loggedIn()
                }
                else {
//                    TODO("Not good... figure something out")
                    setUpGoogleAccountCredential( null )
                    Log.i( TAG, "Silent log in is not enough, trying full login" )
                    (context.value as MainActivity).notLoggedIn()
                }
            }
        }
    }

    private val TAG = "AuthenticationManager"
    var signInAccount : GoogleSignInAccount? = null

}