/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import android.util.Log
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.services.sheets.v4.Sheets
import com.google.api.services.sheets.v4.model.ValueRange
import de.dullhaus.beerdb.util.Config
import de.dullhaus.beerdb.util.Utility
import kotlinx.coroutines.*

class SimpleSheetsData(
    private val transport : HttpTransport,
    private val jsonFactory: JsonFactory ) {


    companion object {
        const val range = "A:L"
        const val appendRange = "A:B"
        private const val TAG = "SimpleSheetsData"
        private const val sheetKey = "15uXwwkcNo-oek8efB8WZt-wkzDyQkXlIb01hq1kG0wQ"
        private const val sheetName = "AllBeers"
        private const val allOfThem = "$sheetName!$range"
        private const val lastModDate = "MetaData!A1"
        private const val adminRange = "MetaData!F:F"
        private val scope = CoroutineScope( Dispatchers.IO + Job() )


        fun beerListFromSheet( vals : List<List<Any>> ) : List<BeerDbData> {
            if( vals.isEmpty() ) return listOf()
            return vals.filter{ it.isNotEmpty() && it.elementAt(0).toString().toIntOrNull(10 ) != null }.map {
                BeerDbData(
                    id = it.elementAt(0).toString().toInt(),
                    name = it.elementAtOrElse(1, { "" }).toString(),
                    brewery = it.elementAtOrElse(2, { "" }).toString(),
                    picUrl = it.elementAtOrElse(3, { "" }).toString(),
                    countryCode = it.elementAtOrElse(4, { "" }).toString(),
                    gravity = Utility.strToDouble( it.elementAtOrElse(5, { "-1.0" } ).toString() ) ,
                    lastChanged = Utility.convStrToZonedDateTime( it.elementAtOrElse(6, { "" } ).toString() ) ,
                    userHash = it.elementAtOrElse(7, { "" }).toString(),
                    adminData = BeerAdminData(vals.indexOf(it) + 1 ), // A1 notation is indexed from 1
                    alcContent = Utility.strToDouble( it.elementAtOrElse(8, { "-1.0" } ).toString() ),
                    type = it.elementAtOrElse(9, { 0 } ).toString().toIntOrNull(10) ?: 0,
                    brewedSince = Utility.convStrToBrewDate( it.elementAtOrElse(10, { "" } ).toString()) ,
                    brewedUntil = Utility.convStrToBrewDate (it.elementAtOrElse(11, { "" } ).toString() ),
                )
            }
        }
    }


    private val sheetsAPI : Sheets
        get() {
            return Sheets.Builder( transport,
                jsonFactory,
                Kraken.authMan.googleAccountCredential )
                .setApplicationName( Data.appName )
                .build()
        }

    private val sheetsApiPubl : Sheets
        get() {
            return Sheets.Builder( transport, jsonFactory, null)
                .setApplicationName( Data.appName )
                .build()
        }



    fun changeBeer( beer : Beer ) {
        scope.launch {
            if( beer.beerDbData.id < 0 ) return@launch
            val row = Data.beerDbLive.value?.find {it.beerDbData.id == beer.beerDbData.id }?.beerDbData?.adminData?.row
                ?: return@launch
            val rangeToChange = "$row:$row"
            val values = ValueRange()
            values.setValues( listOf( beer.beerDbData.asList() ) )
            sheetsAPI.spreadsheets().values().update( sheetKey, rangeToChange, values )
                .setValueInputOption("RAW").execute()
        }
    }

    fun addBeer( beer : Beer ) {
        scope.launch {
            if( beer.beerDbData.id >= 0 ) return@launch
            val values = ValueRange()
            val list = beer.beerDbData.asList().toMutableList()
            list[0] = ""
            values.setValues( listOf( list ) )
            sheetsAPI.spreadsheets().values().append( sheetKey, appendRange, values )
                .setValueInputOption("RAW").execute()
        }
    }



    suspend fun syncBeerDbData( cachedDate : String ) {
        withContext( Dispatchers.IO ) {
            var currentDateStr = cachedDate
            try {
                // I have no clue why but it feels better to use the same auth method like everywhere else
                val responseDate : ValueRange = if( !Config.modifyAccess ) {
                        sheetsApiPubl.spreadsheets().values()
                            .get(sheetKey, lastModDate)
                            .setKey( Data.apiKey )
                            .execute()
                    }
                    else {
                        sheetsAPI.spreadsheets().values()
                            .get(sheetKey, lastModDate)
                            .execute()
                    }
                currentDateStr = responseDate.getValues()[0].toString()
            }
            catch ( ex : Exception )
            {
                Log.w(TAG, "Something is wrong here: $ex!")
            }


            if( currentDateStr != cachedDate ) {
                val response = if( !Config.modifyAccess ) {
                        sheetsApiPubl.spreadsheets().values()
                            .batchGet(sheetKey)
                            .setRanges(listOf(allOfThem, adminRange))
                            .setKey(Data.apiKey)
                            .execute()
                    }
                    else {
                        sheetsAPI.spreadsheets().values()
                            .batchGet(sheetKey)
                            .setRanges(listOf(allOfThem, adminRange))
                            .execute()
                    }

                val beerResponse = response.valueRanges[0].getValues()

                val beerList = beerListFromSheet( beerResponse )

                val adminList = response.valueRanges[1].getValues().map{ it[0].toString() }

                Data.newBeerDbData( currentDateStr, beerList, adminList )
            }
        }
    }

}