/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.dullhaus.beerdb.R

class BoringFragment : Fragment() {

    override fun onCreateView( inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle? ) : View? {
        return inflater.inflate(R.layout.fragment_tab, container, true)
    }

    var userData = false
    var modRequests = false

    var trueChild : BeerListFragment ? = null
    var childsOpinion : BeerListFragment ? = null

    override fun onViewCreated(view : View, savedInstanceState : Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val fragCont = view.findViewById<FragmentContainerView>(R.id.single_tab_frag_cont)
        // TODO: Remove soooon
        val frag = childFragmentManager.fragments[0].childFragmentManager.fragments[0]
        if( frag is BeerListFragment )
        {
            trueChild = frag
            frag.userData = userData
            frag.modRequests = modRequests
        }
        if( childsOpinion != null && trueChild == null ) {
            trueChild = childsOpinion as BeerListFragment
        }

    }
}