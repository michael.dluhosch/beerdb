/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *       http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main.dialogs

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import de.dullhaus.beerdb.R

// TODO:  Use Interface instead of function pointer
class ModifyGoogle( private val yes : () -> Unit ) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setMessage( requireContext().getString( R.string.modifyPermQ ) )
                .setPositiveButton( requireContext().getString( R.string.yes ),
                    DialogInterface.OnClickListener { _, _ ->
                        yes()
                    })
                .setNegativeButton( requireContext().getString( R.string.no ),
                    DialogInterface.OnClickListener { _, _ ->
                        // User cancelled the dialog
                    })
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}
