/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.hbb20.CountryCodePicker
import de.dullhaus.beerdb.Beer
import de.dullhaus.beerdb.Data
import de.dullhaus.beerdb.R
import de.dullhaus.beerdb.databinding.BeerItemModifyBinding
import de.dullhaus.beerdb.util.StyleMapper
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class AdminFragment : BeerIModifyFragment() {
    private var _binding: BeerItemModifyBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val beerVM : BeerViewModel by viewModels()

    val args: AdminFragmentArgs by navArgs()

    override fun onCreateView(
        inflater : LayoutInflater,
        container : ViewGroup?,
        savedInstanceState : Bundle?
    ) : View {

        _binding = BeerItemModifyBinding.inflate( inflater, container, false )

        commonViewInit( binding )

        val acceptButton = binding.modifyButtonRight
        val rejectButton = binding.modifyButtonLeft
        val closeButton = binding.modifyButtonMiddle

        acceptButton.visibility = View.VISIBLE
        rejectButton.visibility = View.VISIBLE
        if( Data.adminMode ) {
            acceptButton.text = getString( R.string.accept )
            rejectButton.text = getString( R.string.reject )
        }
        else {
            acceptButton.text = getString( R.string.save )
            rejectButton.text = getString( R.string.delete )
        }

        closeButton.visibility = View.VISIBLE
        closeButton.text = getString( R.string.close )

        acceptButton.setOnClickListener {
            beerVM.adminAccept( binding )
            findNavController().popBackStack()
        }

        closeButton.setOnClickListener {
            findNavController().popBackStack()
        }

        rejectButton.setOnClickListener {
            beerVM.clearModRequest()
            findNavController().popBackStack()
        }

        return binding.root
    }

    override fun onViewCreated(view : View, savedInstanceState : Bundle?) {
        // Intentionally commented out because this class is derived from BeerIModifyFragment where we don't want to call the
        // onViewCreated
//        super.onViewCreated(view, savedInstanceState)
        beerVM.setBeer( args.beer )
        beerVM.beerLiveD.observe( viewLifecycleOwner ) { beer ->
            setBeer( binding, beer )
        }
    }

    override fun setBeer(vB : BeerItemModifyBinding, beer : Beer ) {
        super.setBeer(vB, beer)
        if( beer.beerDbData.id >= 0 ) {
            val dbBeer = Data.beerDbLive.value?.find { it.beerDbData.id == beer.beerDbData.id }
            if( dbBeer != null ) {
                if( beer.beerDbData.name != dbBeer.beerDbData.name )
                    vB.modifyNameLbl.text = getString( R.string.name_changed, dbBeer.beerDbData.name )
                if( beer.beerDbData.brewery != dbBeer.beerDbData.brewery )
                    vB.modifyBreweryLbl.text = getString( R.string.brewery_changed, dbBeer.beerDbData.brewery )
                if( beer.beerDbData.countryCode != dbBeer.beerDbData.countryCode ) {
                    val ccp = CountryCodePicker( requireContext() )
                    ccp.setCountryForNameCode(  dbBeer.beerDbData.countryCode )
                    try {
                        vB.modifyCountryLbl.text = getString( R.string.country_changed, ccp.selectedCountryName )
                    }
                    catch( ex : NullPointerException ) {
                        vB.modifyCountryLbl.text = getString( R.string.country_changed, "No Data" )
                    }
                }
                if( beer.beerDbData.type != dbBeer.beerDbData.type )
                    vB.modifyTypeLbl.text = getString( R.string.type_changed, StyleMapper.getDetailedName(  dbBeer.beerDbData.type ) )
                if( beer.beerDbData.alcContent != dbBeer.beerDbData.alcContent )
                    vB.modifyAlcLbl.text = getString( R.string.alc_changed, dbBeer.beerDbData.alcContent.toString() )
                if( beer.beerDbData.gravity != dbBeer.beerDbData.gravity )
                    vB.modifyGravityLbl.text = getString( R.string.gravity_changed, dbBeer.beerDbData.gravity.toString() )
                if( beer.beerDbData.brewedSince != dbBeer.beerDbData.brewedSince )
                    vB.modifyBrewedSinceLbl.text = getString( R.string.brewed_since_changed,
                        dbBeer.beerDbData.brewedSince?.format( DateTimeFormatter.ofLocalizedDate( FormatStyle.MEDIUM ) ) )
                if( beer.beerDbData.brewedUntil != dbBeer.beerDbData.brewedUntil )
                    vB.modifyBrewedUntilLbl2.text = getString( R.string.brewed_until_changed,
                        dbBeer.beerDbData.brewedUntil?.format( DateTimeFormatter.ofLocalizedDate( FormatStyle.MEDIUM ) ) )
            }
        }
        vB.myDataLbl.visibility = View.GONE
        vB.modifyCommentLbl.visibility = View.GONE
        vB.modifyComment.visibility = View.GONE
    }
}
