/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import de.dullhaus.beerdb.Beer
import de.dullhaus.beerdb.databinding.BeerItemBinding
import de.dullhaus.beerdb.util.onBeerClickListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class RecyclerAdapter( private val context : Context,
                       private val userData : Boolean,
                       private val itemClickListener : onBeerClickListener )
    : RecyclerView.Adapter< BeerViewHolder >() {

    val sortedList = SortedList(Beer::class.java, object :  SortedListAdapterCallback<Beer>(this) {
        override fun compare(o1 : Beer, o2 : Beer) : Int {
            if( userData )
            {
                val tmp1 = o1.userData
                val tmp2 = o2.userData
                if( tmp1 != null && tmp2 != null ) {
                    return tmp2.dateAdded.compareTo( tmp1.dateAdded )
                }
            }
            // Just show the latest beer from the database (The IDs are monotonically increasing)
            return o2.beerDbData.id.compareTo(o1.beerDbData.id)
        }

        override fun areContentsTheSame(oldItem : Beer, newItem : Beer) : Boolean {
            return oldItem.beerDbData.contentsTheSame( newItem.beerDbData )
        }

        override fun areItemsTheSame(item1 : Beer, item2 : Beer) : Boolean {
            return item1.beerDbData.id == item2.beerDbData.id
        }

    } )
    private var filterStr = ""
    private var beers : List<Beer> = listOf()
    private val scope = CoroutineScope( Dispatchers.Main + Job() )

    private val TAG = "RecyclerAdapter"

    override fun onCreateViewHolder( parent: ViewGroup, viewType: Int ) : BeerViewHolder {
        return BeerViewHolder(
            BeerItemBinding.inflate( LayoutInflater.from( context ), parent, false ) )
    }

    override fun getItemCount(): Int {
        return sortedList.size()
    }

    val obsi = Observer<MutableList<Beer>> { liveBeers ->
        beers = if( userData ) liveBeers?.filter { it.userData != null } ?: listOf()
            else liveBeers ?: listOf()

        doFiltering( filterStr )
    }

    private var filterJob : Job ? = null
    fun doFiltering(str : String ?) {
        // If the sortedList is zero there seems to be a bug that the RecyclerView ignores the notifyDataSetChanged call!
        // It can also be fixed by a delay so that is longer 0 and actually visible to the user
        // I decided for filling the list with all the beers to show something which then later hopefully gets filtered
        if( sortedList.size() == 0 && beers.isNotEmpty() ) sortedList.addAll( beers )

        filterStr = str ?: ""
        filterJob?.cancel()
        filterJob = scope.launch {
            val subList =
                if(filterStr.isNotEmpty()) {
                    beers.filter{
                        it.beerDbData.name.contains( filterStr, true ) ||
                        it.beerDbData.brewery.contains( filterStr, true )
                    }
                }
                else beers
            sortedList.replaceAll( subList.toTypedArray(), true )
        }.also {
            it.invokeOnCompletion {
                notifyDataSetChanged()
            }
        }
    }

    override fun onBindViewHolder( holder: BeerViewHolder, position: Int ) {
        holder.bindListener( sortedList[position], itemClickListener )
        holder.bindBeer( sortedList[position], position, context)
    }

}
