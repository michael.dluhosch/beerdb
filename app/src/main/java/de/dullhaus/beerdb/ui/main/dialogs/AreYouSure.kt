/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main.dialogs

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import de.dullhaus.beerdb.Data
import de.dullhaus.beerdb.Kraken
import de.dullhaus.beerdb.R
import de.dullhaus.beerdb.ui.main.BeerClickedFragment

class AreYouSure() : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setMessage( requireContext().getString( R.string.removeBeerQ ) )
                .setPositiveButton( requireContext().getString( R.string.yes ),
                    DialogInterface.OnClickListener { _, _ ->
                        val leFrag = Kraken.sectionsPagerAdapter.getMyBeerFragment()?.parentFragmentManager?.fragments?.firstOrNull()
                        if( leFrag != null && leFrag is BeerClickedFragment ) {
                            Data.removeBeerFromUserBeers( leFrag.args.beerArg )
                            leFrag.findNavController().popBackStack()
                        }
                    })
                .setNegativeButton( requireContext().getString( R.string.no ),
                    DialogInterface.OnClickListener { _, _ ->
                        // User cancelled the dialog
                    })
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}
