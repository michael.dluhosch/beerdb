/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main.dialogs

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.Html
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import de.dullhaus.beerdb.R

class Terms() : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {

            val spannable = SpannableString( Html.fromHtml( getString(
                    R.string.terms_and_privacy,
                    getString( R.string.terms_url),
                    getString( R.string.terms_of_use),
                    getString( R.string.privacy_url),
                    getString( R.string.privacy_policy),
                    )
                , Html.FROM_HTML_MODE_LEGACY ) )

            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it, android.R.style.Theme_Material_Light_Dialog_Alert )
            builder.setTitle( requireContext().getString( R.string.terms_and_conditions ))
                .setMessage( spannable )
                .setPositiveButton( requireContext().getString( R.string.close ),
                    DialogInterface.OnClickListener { _, _ ->
                    } )
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onStart() {
        super.onStart()
        dialog?.findViewById<TextView>(android.R.id.message)?.movementMethod = LinkMovementMethod.getInstance()
    }
}
