/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import de.dullhaus.beerdb.Beer
import de.dullhaus.beerdb.Data
import de.dullhaus.beerdb.Kraken
import de.dullhaus.beerdb.R
import de.dullhaus.beerdb.databinding.BeerListFragmentBinding
import de.dullhaus.beerdb.ui.main.dialogs.ModifyGoogle
import de.dullhaus.beerdb.util.Config
import de.dullhaus.beerdb.util.Utility
import de.dullhaus.beerdb.util.onBeerClickListener
import kotlinx.coroutines.*

class BeerListFragment()
    : Fragment(),
    onBeerClickListener
{
    private val TAG = "PlaceholderFragment"
    var rcycAdapt : RecyclerAdapter? = null
    lateinit var recyclerView : RecyclerView

    private var _binding: BeerListFragmentBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    var userData = false
    var modRequests = false

    val searchListener = object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query : String?) : Boolean {
            return false
        }

        override fun onQueryTextChange(newText : String?) : Boolean {
            rcycAdapt?.doFiltering(newText)
            return false
        }
    }
    lateinit var searchView : androidx.appcompat.widget.SearchView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val grandDad = parentFragment?.parentFragment
        if( grandDad is BoringFragment )
        {
            if ( grandDad.childsOpinion != this ) grandDad.childsOpinion = this
            userData = grandDad.userData
            modRequests = grandDad.modRequests
        }
    }

    override fun onItemClicked( beer : Beer ) {
        if( Kraken.viewPager.currentItem == 2 ) {
            val dir = BeerListFragmentDirections.actionBeerClickedAdmin( beer )
            findNavController().navigate( dir )
        }
        else {
            val dir = BeerListFragmentDirections.actionBeerClicked( beer )
            findNavController().navigate( dir )
        }
    }

    private lateinit var layoutMgr : LinearLayoutManager
    fun getLayoutMgr() : LinearLayoutManager? {
        return if( this::layoutMgr.isInitialized ) layoutMgr else null
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {

        _binding = BeerListFragmentBinding.inflate( inflater, container, false )
        return binding.root
    }

    override fun onViewCreated(view : View, savedInstanceState : Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rcycAdapt = RecyclerAdapter( requireContext(), userData, this )

        recyclerView = binding.recyclerView
        layoutMgr = LinearLayoutManager( requireContext() )
        recyclerView.apply {
            layoutManager = layoutMgr
            adapter = rcycAdapt
        }

        if( modRequests ) Data.modRequestsLive.observe( viewLifecycleOwner, rcycAdapt!!.obsi )
        else Data.beerDbLive.observe(viewLifecycleOwner, rcycAdapt!!.obsi)

        val scopeUi = CoroutineScope( Dispatchers.Main + Job() )
        binding.swipeContainer.setOnRefreshListener {
            scopeUi.launch {
                try {
                    Data.handleRefresh( requireContext() )
                }
                finally {
                    binding.swipeContainer.isRefreshing = false
                }
            }
        }

        val fab: FloatingActionButton = binding.floatingActionButton

        if( userData || modRequests ) {
            fab.hide()
        }

        fab.setOnClickListener {
            if( Config.modifyAccess ) {
                val dir = BeerListFragmentDirections.actionNewBeerClicked( true )
                findNavController().navigate( dir )
            }
            else {
                ModifyGoogle( ::userAccepted ).show( parentFragmentManager, "rofl" )
            }
        }

        lifecycleScope.launch {
            Kraken.initJob.join()
            val searchItem = Kraken.topAppBar.menu.findItem(R.id.search)
            searchView = searchItem.actionView as androidx.appcompat.widget.SearchView

            searchView.setOnQueryTextListener( searchListener )
        }
    }

    override fun onResume() {
        super.onResume()
        if( ::searchView.isInitialized ) {
            searchView.setOnQueryTextListener( searchListener )
            if( searchView.query.isNotEmpty() ) rcycAdapt?.doFiltering(searchView.query.toString())
        }
    }

    override fun onPause() {
        super.onPause()
        if( ::searchView.isInitialized ) {
            searchView.setOnQueryTextListener(null)
        }
    }

    private fun userAccepted() {
        Config.modifyAccess = true
        Kraken.goToNewBeer = true
        Kraken.authMan.applyScopes( Utility.scopes )
        Kraken.authMan.startLogin()
    }

}
