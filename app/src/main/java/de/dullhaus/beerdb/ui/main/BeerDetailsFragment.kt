/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *       http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import de.dullhaus.beerdb.Beer
import de.dullhaus.beerdb.R
import de.dullhaus.beerdb.UserBeer
import de.dullhaus.beerdb.databinding.BeerItemDetailsBinding
import de.dullhaus.beerdb.util.StyleMapper
import de.dullhaus.beerdb.util.Utility
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class BeerDetailsFragment : Fragment() {

    val beerVM : BeerViewModel by viewModels()

    private var _binding: BeerItemDetailsBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater : LayoutInflater,
        container : ViewGroup?,
        savedInstanceState : Bundle?
    ) : View {

        _binding = BeerItemDetailsBinding.inflate( inflater, container, false )
        return binding.root
    }

    override fun onViewCreated(view : View, savedInstanceState : Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.plusConsumeButton.setOnClickListener {
            beerVM.incConsume()
        }

        binding.minusConsumeButton.setOnClickListener {
            beerVM.decConsume()
        }

        binding.root.doOnPreDraw {
            magic( binding )
        }

        beerVM.beerLiveD.observe( viewLifecycleOwner ) { beer ->
            setDataDB( beer )

            val userDataVisibility = if( beer.userData == null ) View.GONE else View.VISIBLE
            binding.myData.visibility = userDataVisibility
            binding.addedOn.visibility = userDataVisibility
            binding.detailAddedOn.visibility = userDataVisibility
            binding.comment.visibility = userDataVisibility
            binding.detailComment.visibility = userDataVisibility
            binding.timesConsumed.visibility = userDataVisibility
            binding.detailTimesConsumed.visibility = userDataVisibility
            binding.plusConsumeButton.visibility = userDataVisibility
            binding.minusConsumeButton.visibility = userDataVisibility
            binding.lastConsumed.visibility = userDataVisibility
            binding.detailLastConsumed.visibility = userDataVisibility

            beer.userData?.let { userData ->
                setDataUser( userData )
            }
        }
    }

    private fun setDataDB( beer : Beer ) {
        binding.detailBeerName.text = beer.beerDbData.name
        binding.detailBrewery.text = beer.beerDbData.brewery
        binding.detailCountry.setCountryForNameCode( beer.beerDbData.countryCode )
        binding.detailType.text = StyleMapper.getDisplayName( beer.beerDbData.type )
        if( beer.beerDbData.alcContent >= 0 ) binding.detailAlcCont.text = getString( R.string.alcPercent, beer.beerDbData.alcContent.toString() )
        else{
            binding.alc.visibility = View.GONE
            binding.detailAlcCont.visibility = View.GONE
        }
        if( beer.beerDbData.gravity >= 0 ) binding.detailGravity.text = getString( R.string.gravityPlato, beer.beerDbData.gravity.toString() )
        else {
            binding.gravity.visibility = View.GONE
            binding.detailGravity.visibility = View.GONE
        }
        if( beer.beerDbData.brewedSince != null )
            binding.detailBrewedSince.text = beer.beerDbData.brewedSince?.format( DateTimeFormatter.ofLocalizedDate( FormatStyle.MEDIUM ) )
        else {
            binding.brewedSince.visibility = View.GONE
            binding.detailBrewedSince.visibility = View.GONE
        }

        if( beer.beerDbData.brewedUntil != null )
            binding.detailBrewedUntil.text = beer.beerDbData.brewedUntil?.format( DateTimeFormatter.ofLocalizedDate( FormatStyle.MEDIUM ) )
        else{
            binding.brewedUntil.visibility = View.GONE
            binding.detailBrewedUntil.visibility = View.GONE
        }

        val detailedType = StyleMapper.getDetailedName( beer.beerDbData.type )
        if( binding.detailType.text.toString() == detailedType ) {
            binding.detailedType.visibility = View.GONE
            binding.detailDetailedType.visibility = View.GONE
        }
        else {
            binding.detailedType.visibility = View.VISIBLE
            binding.detailDetailedType.visibility = View.VISIBLE
            binding.detailDetailedType.text = detailedType
        }

        Utility.loadPic( beer, binding.detailImage )
    }

    private fun setDataUser( userData : UserBeer ) {
        binding.detailTimesConsumed.text = userData.timesConsumed.toString()
        var localTime = userData.dateLastConsumed.withZoneSameInstant( ZoneId.systemDefault() ).toLocalDateTime()
        binding.detailLastConsumed.text = localTime.format( DateTimeFormatter.ofLocalizedDateTime( FormatStyle.MEDIUM ) )
        localTime = userData.dateAdded.withZoneSameInstant( ZoneId.systemDefault() ).toLocalDateTime()
        binding.detailAddedOn.text = localTime.format( DateTimeFormatter.ofLocalizedDateTime( FormatStyle.MEDIUM ) )
        if( userData.comment.isNotEmpty() ) binding.detailComment.text = userData.comment
        else {
            binding.comment.visibility = View.GONE
            binding.detailComment.visibility = View.GONE
        }
    }

    // TODO: Compose?
    // Adjusts the constraints based on current layout information
    private fun magic(vB : BeerItemDetailsBinding ) {
        // first we need a map of all the views fom top to bottom (fortunately kotlin mapOf preserves order)
        val map = mapOf(
            vB.name to vB.detailBeerName,
            vB.brewery to vB.detailBrewery,
            vB.country to vB.detailCountry,
            vB.type to vB.detailType,
            vB.alc to vB.detailAlcCont,
            vB.detailedType to vB.detailDetailedType,
            vB.gravity to vB.detailGravity,
            vB.brewedSince to vB.detailBrewedSince,
            vB.brewedUntil to vB.detailBrewedUntil,
            vB.myData to vB.detailTimesConsumed,
            vB.lastConsumed to vB.detailLastConsumed,
            vB.addedOn to vB.detailAddedOn,
            vB.comment to vB.detailComment
        )

        val cl = vB.elConstraints
        val cs = ConstraintSet()
        cs.clone(cl)

        var foundIt = false
        for( elmt in map ) {
            // When the first view is below the image we want to change constraints
            val belowImage = !foundIt && elmt.value.bottom > vB.detailImage.bottom
            val arr1 = intArrayOf( 0, 0 )
            val arr2 = intArrayOf( 0, 0 )
            val arr3 = intArrayOf( 0, 0 )
            elmt.value.getLocationOnScreen( arr1 )
            elmt.value.getLocationInWindow( arr2 )
            vB.detailImage.getLocationInWindow( arr3 )

            // if this beer does not contain a lot of information then it could be that even the user data section
            // can fit right next to the picture. This is something which looks weird so it is forbidden here
            val userDataAbove = !foundIt && elmt.key == vB.myData


            if( belowImage || userDataAbove ) {
                cs.connect( elmt.key.id, ConstraintSet.TOP, vB.detailImage.id, ConstraintSet.BOTTOM )
                foundIt = true
            }

            if( foundIt ) {
                cs.constrainPercentWidth( elmt.key.id , 1.0F )
                cs.constrainPercentWidth( elmt.value.id , 1.0F )
            }
            else {
                cs.constrainPercentWidth( elmt.key.id , 0.5F )
                cs.constrainPercentWidth( elmt.value.id , 0.5F )
            }
        }

        cs.applyTo( cl )

        requireView().invalidate()
    }
}
