/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import android.widget.EditText
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.preference.PreferenceManager
import de.dullhaus.beerdb.Beer
import de.dullhaus.beerdb.Data
import de.dullhaus.beerdb.R
import de.dullhaus.beerdb.databinding.BeerItemModifyBinding
import de.dullhaus.beerdb.util.*
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

open class BeerIModifyFragment : Fragment() {
    private var _binding: BeerItemModifyBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val args: BeerIModifyFragmentArgs by navArgs()

    private lateinit var customBackPresser : OnBackPressedCallback

    private val beerVM : BeerViewModel by viewModels()

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        // with the current design of the navigation graph we need to intercept the backpress :(
        customBackPresser = requireActivity().onBackPressedDispatcher.addCallback(this) {
            findNavController().popBackStack()
        }
        customBackPresser.isEnabled = true
    }

    protected fun commonViewInit( vB : BeerItemModifyBinding ) {
        prepEditText( vB.modifyPicUrl, activity )

        val prefs = PreferenceManager.getDefaultSharedPreferences( requireContext() )
        vB.switchFilterType.isChecked = prefs.getBoolean( Config.regionalBeerTypeFilter, true )

        val aaRegional = ArrayAdapter( requireContext(), android.R.layout.simple_spinner_item,  StyleMapper.regionalStylesList ).also {
            it.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item )
        }
        val aaAll = ArrayAdapter( requireContext(), android.R.layout.simple_spinner_item,  StyleMapper.allStylesList ).also {
            it.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item )
        }

        val setAdapter = { regional : Boolean ->
            if( regional )
                vB.modifyType.adapter = aaRegional
            else
                vB.modifyType.adapter = aaAll
        }

        setAdapter( vB.switchFilterType.isChecked )

        vB.switchFilterType.setOnCheckedChangeListener { _, isChecked ->
            prefs.edit().putBoolean( Config.regionalBeerTypeFilter, isChecked ).apply()
            setAdapter( isChecked )
            vB.modifyType.setSelection( StyleMapper.getSpinnerId( beerVM.beerLiveD.value!!.beerDbData.type ) )
        }

        vB.modifyCountry.setCountryPreference( Data.countryPref )


        val defaultBackground = vB.modifyBrewedSince.background

        // pass by ref would be nice...
        class DateTextStyler(val etxt : EditText ) : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView : CompoundButton?, isChecked : Boolean) {
                if( isChecked ) {
                    etxt.setOnClickListener( null )
                    etxt.setBackgroundColor( Color.GRAY )
                    etxt.background.alpha = 127
                    etxt.setText( "" )
                }
                else {
                    etxt.setOnClickListener { DatePickers.show( it, parentFragmentManager, beerVM ) }
                    etxt.background = defaultBackground
                }
            }
        }

        vB.checkBoxNoData.setOnCheckedChangeListener( DateTextStyler( vB.modifyBrewedSince ) )
        vB.checkBoxActivilyBrewed.setOnCheckedChangeListener( DateTextStyler( vB.modifyBrewedUntil ) )

        if( !vB.checkBoxNoData.isChecked )
            vB.modifyBrewedSince.setOnClickListener { DatePickers.show( it, parentFragmentManager, beerVM ) }

        if( !vB.checkBoxActivilyBrewed.isChecked )
            vB.modifyBrewedUntil.setOnClickListener { DatePickers.show( it, parentFragmentManager, beerVM ) }

        vB.termsCondTv.visibility = View.GONE
    }

    override fun onCreateView(
        inflater : LayoutInflater,
        container : ViewGroup?,
        savedInstanceState : Bundle?
    ) : View {
        _binding = BeerItemModifyBinding.inflate( inflater, container, false )
        return binding.root
    }

    override fun onViewCreated(view : View, savedInstanceState : Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        commonViewInit( binding )

        val saveButton = binding.modifyButtonRight
        val closeButton = binding.modifyButtonMiddle
        saveButton.visibility = View.VISIBLE
        saveButton.text = getString( R.string.save )
        closeButton.visibility = View.VISIBLE
        closeButton.text = getString( R.string.close )


        binding.termsCondTv.visibility = View.VISIBLE
        binding.termsCondTv.text =
            Html.fromHtml( getString(
                R.string.terms_notify,
                getString( R.string.save ),
                getString( R.string.terms_url ),
                getString( R.string.terms_of_use )
            )
                , Html.FROM_HTML_MODE_LEGACY )

        binding.termsCondTv.movementMethod = LinkMovementMethod.getInstance()

        saveButton.setOnClickListener {
            beerVM.saveUserChanges( binding, args.beer )

            // Make sure the keyboard is hidden
            val imm : InputMethodManager = requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
            findNavController().popBackStack()
        }

        closeButton.setOnClickListener {
            // Make sure the keyboard is hidden
            val imm : InputMethodManager = requireContext().getSystemService( Activity.INPUT_METHOD_SERVICE ) as InputMethodManager
            imm.hideSoftInputFromWindow( binding.root.windowToken, 0 )
            findNavController().popBackStack()
        }

        beerVM.beerLiveD.observe( viewLifecycleOwner ) { beer ->
            setBeer( binding, beer )
        }

        if( args.newBeerArg && beerVM.beerLiveD.value == null ) beerVM.setBeer( Beer() )
        args.beer?.let { beerVM.setBeer( it.copy() ) }
    }

    override fun onPause() {
        super.onPause()
        customBackPresser.isEnabled = false
    }

    override fun onResume() {
        super.onResume()
        customBackPresser.isEnabled = true
    }

    open fun setBeer( vB : BeerItemModifyBinding, beer : Beer ) {
        if( beer.beerDbData.id < 0 ) {
            vB.newBeerTv.text = getString( R.string.newBeer )
        }
        else {
            vB.newBeerTv.text = getString( R.string.modification )
        }

        // Commented out because it probably don't hurt and it's needed for AdminFragment
//        if( beer.beerDbData.id < 0  ) return
        vB.modifyPicUrl.setText(beer.beerDbData.picUrl)
        vB.modifyName.setText(beer.beerDbData.name)
        vB.modifyBrewery.setText( beer.beerDbData.brewery )
        vB.modifyCountry.setCountryForNameCode(beer.beerDbData.countryCode)
        vB.modifyType.setSelection( StyleMapper.getSpinnerId( beer.beerDbData.type ) )
        if( beer.beerDbData.alcContent >= 0 ) vB.modifyAlcCont.setText( getString( R.string.alcPercent, beer.beerDbData.alcContent.toString() ) )
        else vB.modifyAlcCont.setText("")
        if( beer.beerDbData.gravity >= 0 ) vB.modifyGravity.setText( getString( R.string.gravityPlato, beer.beerDbData.gravity.toString() ) )
        else vB.modifyGravity.setText("")

        if( beer.beerDbData.brewedSince == null ) {
            vB.checkBoxNoData.isChecked = true
        }
        else {
            vB.modifyBrewedSince.setText( beer.beerDbData.brewedSince?.format( DateTimeFormatter.ofLocalizedDate( FormatStyle.MEDIUM ) ) )
            vB.checkBoxNoData.isChecked = false
        }

        if( beer.beerDbData.brewedUntil == null ) {
            vB.checkBoxActivilyBrewed.isChecked = true
        }
        else {
            vB.modifyBrewedUntil.setText( beer.beerDbData.brewedUntil?.format( DateTimeFormatter.ofLocalizedDate( FormatStyle.MEDIUM ) ) )
            vB.checkBoxActivilyBrewed.isChecked = false
        }
        // User Data
        vB.modifyComment.setText( beer.userData?.comment )

        val userDataVisibility = if( beer.userData == null ) View.GONE else View.VISIBLE
        vB.myDataLbl.visibility = userDataVisibility
        vB.modifyCommentLbl.visibility = userDataVisibility
        vB.modifyComment.visibility = userDataVisibility

        Utility.loadPic( beer, vB.modifyImage )
    }

}
