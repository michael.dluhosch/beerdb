/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import de.dullhaus.beerdb.Beer
import de.dullhaus.beerdb.Data
import de.dullhaus.beerdb.databinding.BeerItemModifyBinding
import de.dullhaus.beerdb.util.StyleMapper
import java.time.ZonedDateTime

class BeerViewModel : ViewModel() {
    private val _beerLiveD = MutableLiveData<Beer>()

    val beerLiveD : LiveData<Beer>
        get() = _beerLiveD

    private lateinit var beerDbObs : Observer<MutableList<Beer>>

    fun setBeer( beer : Beer ) {
        _beerLiveD.value = beer
    }

    fun incConsume() {
        _beerLiveD.value?.userData?.timesConsumed = _beerLiveD.value?.userData?.timesConsumed?.inc()!!
        _beerLiveD.value?.userData?.dateLastConsumed = ZonedDateTime.now()

//        beerLiveD.value = beerLiveD.value // tell the observers something changed...
        Data.updateUserBeer()
    }

    fun decConsume() {
        if( _beerLiveD.value?.userData?.timesConsumed!! > 0 ) {
            _beerLiveD.value?.userData?.timesConsumed = _beerLiveD.value?.userData?.timesConsumed?.dec()!!

//            beerLiveD.value = beerLiveD.value // tell the observers something changed...
            Data.updateUserBeer()
        }
    }

    private fun saveDataToBeer( ui : BeerItemModifyBinding ) {
        _beerLiveD.value?.let { beer ->
            beer.beerDbData.picUrl = ui.modifyPicUrl.text.toString()
            beer.beerDbData.name = ui.modifyName.text.toString()
            beer.beerDbData.brewery = ui.modifyBrewery.text.toString()
            beer.beerDbData.countryCode = ui.modifyCountry.selectedCountryNameCode ?: ""
            val str = if(ui.modifyType.selectedItem is String) ui.modifyType.selectedItem as String else Data.beerStr
            beer.beerDbData.type = StyleMapper.getIdFromName(str)
            beer.beerDbData.alcContent = ui.modifyAlcCont.text.toString().removeSuffix("%").trim().toDoubleOrNull() ?: -1.0
            beer.beerDbData.gravity = ui.modifyGravity.text.toString().removeSuffix("°P").trim().toDoubleOrNull() ?: -1.0
            if( ui.modifyBrewedSince.text.isEmpty() ) beer.beerDbData.brewedSince = null
            if( ui.modifyBrewedUntil.text.isEmpty() ) beer.beerDbData.brewedUntil = null

            // User Data
            beer.userData?.comment = ui.modifyComment.text.toString()
        }
    }

    fun saveUserChanges(ui : BeerItemModifyBinding, dbBeer : Beer? ) {
        _beerLiveD.value?.let { beer ->
            val oldBeer = dbBeer ?: beer.copy()
            saveDataToBeer(ui)
            if( !beer.beerDbData.contentsTheSame( oldBeer.beerDbData ) ) {
                beer.beerDbData.userHash = Data.userHash
                beer.beerDbData.lastChanged = ZonedDateTime.now()
                Data.addModRequest( beer )
            }

            if( beer.userData != oldBeer.userData )
            {
                dbBeer?.userData = beer.userData
                Data.updateUserBeer( )
            }
        }
    }

    fun adminAccept( ui : BeerItemModifyBinding ) {
        saveDataToBeer(ui)
        _beerLiveD.value?.let { beer ->
            if(Data.adminMode) Data.acceptModRequest(beer)
            else Data.changeModRequest(beer)
        }
    }

    fun clearModRequest() = Data.clearModRequest( _beerLiveD.value!! )

}
