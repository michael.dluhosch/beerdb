/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import de.dullhaus.beerdb.Beer
import de.dullhaus.beerdb.BeerPicCache
import de.dullhaus.beerdb.R
import de.dullhaus.beerdb.databinding.BeerItemBinding
import de.dullhaus.beerdb.util.onBeerClickListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class BeerViewHolder( private val v : BeerItemBinding ) : RecyclerView.ViewHolder( v.root ) {
    private val beerName : TextView = v.tvBeerName
    private val beerBrewery : TextView = v.tvBeerBrewery
    private val beerPic : ImageView = v.ivBeerIcon
    private val beerPicTxt : TextView = v.tvBeerIconBackupText

    fun bindListener(beer: Beer, clickListener: onBeerClickListener) {
        v.root.setOnClickListener{ clickListener.onItemClicked( beer ) }
    }

    private val scope = CoroutineScope( Dispatchers.Main + Job() )

    fun bindBeer( beer : Beer, position : Int, context : Context ) {
        beerName.text = beer.beerDbData.name
        beerBrewery.text = beer.beerDbData.brewery

        scope.launch {
            // TODO Make image blurry or other effect to show we are loading
            val elBitmap = BeerPicCache.getPic(beer.beerDbData.picUrl)

            if( position != bindingAdapterPosition ) {
                return@launch
            }

            if(elBitmap != null) {
                beerPic.setImageBitmap(elBitmap)
                beerPicTxt.visibility = View.INVISIBLE
            } else {
                beerPic.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.ic_beer_bottle_plain_shape,
                        null
                    )
                )
                beerPicTxt.visibility = View.VISIBLE
                beerPicTxt.text = beer.beerDbData.name[0].uppercaseChar().toString()
            }
        }
    }
}
