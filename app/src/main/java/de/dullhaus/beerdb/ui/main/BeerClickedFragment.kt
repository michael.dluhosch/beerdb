/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main

import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.dullhaus.beerdb.Beer
import de.dullhaus.beerdb.Data
import de.dullhaus.beerdb.Kraken
import de.dullhaus.beerdb.R
import de.dullhaus.beerdb.databinding.BeerDetailFragmentBinding
import de.dullhaus.beerdb.ui.main.dialogs.ModifyGoogle
import de.dullhaus.beerdb.ui.main.dialogs.UserGoogle
import de.dullhaus.beerdb.util.Config
import de.dullhaus.beerdb.util.Utility

class BeerClickedFragment : Fragment() {

    private var _binding: BeerDetailFragmentBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var contextButton : Button
    private lateinit var modifyButton : Button
    private lateinit var customBackPresser : OnBackPressedCallback

    val args: BeerClickedFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        // with the current design of the navigation graph we need to intercept the backpress :(
        customBackPresser = requireActivity().onBackPressedDispatcher.addCallback(this) {
            findNavController().popBackStack()
        }
        customBackPresser.isEnabled = true
    }

    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle?) : View {
        _binding = BeerDetailFragmentBinding.inflate( inflater, container, false )
        return binding.root
    }

    override fun onViewCreated(view : View, savedInstanceState : Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val beerItemDetailFragment = childFragmentManager.findFragmentByTag( "my_tag") as BeerDetailsFragment

        val closeButton = binding.detailClose
        closeButton.setOnClickListener {
            findNavController().popBackStack()
        }

        contextButton = binding.detailContextButton
        if( Kraken.viewPager.currentItem == 0 ) {
            contextButton.visibility = View.INVISIBLE
        }
        else {
            contextButton.text = getString( R.string.add )
            contextButton.visibility = View.VISIBLE
        }

        contextButton.setOnClickListener {
            if( Kraken.viewPager.currentItem == 1 ) {
                // Add
                if( !Config.userSheetAccess )
                    UserGoogle(::userAddAccepted).show( parentFragmentManager, "rofl" )
                else
                    Data.addBeerToUserBeers( beerItemDetailFragment.beerVM.beerLiveD.value ?: args.beerArg )
            }
        }

        modifyButton = binding.detailModify
        modifyButton.setOnClickListener {
            if( Config.modifyAccess ) {
                val dir = BeerClickedFragmentDirections.actionModifyExistingBeer( false, args.beerArg )
                findNavController().navigate( dir )
            }
            else
                ModifyGoogle( ::userModAccepted ).show( parentFragmentManager, "rofl" )
        }

        val beerDbObs = Observer<MutableList<Beer>> {
            if( args.beerArg.beerDbData.id < 0 ) return@Observer
            val beerDbBeer = it.find{ liveBeer -> liveBeer.beerDbData.id == args.beerArg.beerDbData.id }
            if( beerDbBeer != null  ) {
                beerItemDetailFragment.beerVM.setBeer( beerDbBeer )
            }
            handleAddButton()
        }

        Data.beerDbLive.observe( viewLifecycleOwner, beerDbObs )

        binding.termsCondTv2.text =
            Html.fromHtml( getString(
                R.string.terms_notify,
                getString( R.string.save ),
                getString( R.string.terms_url ),
                getString( R.string.terms_of_use )
            )
                , Html.FROM_HTML_MODE_LEGACY )
        binding.termsCondTv2.movementMethod = LinkMovementMethod.getInstance()

        var szOld = 0
        binding.root.addOnLayoutChangeListener { v, _, top, _, bottom, _, _, _, _ ->
            val sz = bottom - top
            if( sz != szOld && szOld != 0 ) {
                val ratio = sz / szOld.toDouble()
                // For some reason the call to visibility can not be done during Layout
                v.doOnPreDraw {
                    // If more than 25% of the view gets obscured then we give the text more room
                    if(ratio < 0.75)  closeButton.visibility = View.GONE
                    else  closeButton.visibility = View.VISIBLE
                }
            }
            szOld = sz
        }

        handleAddButton()

    }

    private fun userAddAccepted() {
        Config.userSheetAccess = true
        Kraken.authMan.applyScopes( Utility.scopes )
        Kraken.beerToUserify = args.beerArg
        Kraken.authMan.startLogin()
    }

    private fun userModAccepted() {
        Config.modifyAccess = true
        Kraken.beerToUserify = args.beerArg
        Kraken.goToModify = true
        Kraken.goToViewPager = Kraken.viewPager.currentItem
        Kraken.authMan.applyScopes( Utility.scopes )
        Kraken.authMan.startLogin()
    }

    private fun handleAddButton() {
        if( args.beerArg.userData != null ) {
            contextButton.isClickable = false
            contextButton.alpha = 0.5f
        }
        else
        {
            contextButton.isClickable = true
            contextButton.alpha = 1f
        }
    }

    override fun onPause() {
        super.onPause()
        customBackPresser.isEnabled = false
        Kraken.topAppBar.menu.findItem(R.id.menu_remove).isVisible = false
    }

    override fun onResume() {
        super.onResume()
        customBackPresser.isEnabled = true
        if( Kraken.viewPager.currentItem == 0 ) {
            Kraken.topAppBar.menu.findItem(R.id.menu_remove).isVisible = true
        }
    }

}