/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import de.dullhaus.beerdb.BeerPicCache
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * A [FragmentStateAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(fm : FragmentActivity)
    : FragmentStateAdapter( fm )
{
    private val TAG = "SectionsPagerAdapter"

    private inner class Frag( val fragment : Fragment, val id : Long )

    private val mainFragments = mutableListOf(
        BoringFragment(),
        BoringFragment()
    )

    private val thirdOne = BoringFragment()

    override fun createFragment(position : Int) : Fragment {
        val borFrag = if( position < 2) mainFragments[position] else thirdOne

        when(position) {
            0 -> {
                borFrag.userData = true
                borFrag.modRequests = false
            }
            1 -> {
                borFrag.userData = false
                borFrag.modRequests = false
            }
            2 -> {
                borFrag.userData = false
                borFrag.modRequests = true
            }
        }

        return borFrag
    }

    override fun containsItem(itemId : Long) : Boolean {
        return mainFragments.size >= itemId
    }

    override fun getItemId(position : Int) : Long {
        return position.toLong()
    }

    fun addAdminOrUserModTab() {
        // Only Add third tab if needed
        if( mainFragments.size == 2 ) {
            mainFragments.add(2, thirdOne )
            notifyDataSetChanged()
        }
    }

    fun removeAdminOrUserModTab() {
        if( mainFragments.remove( thirdOne ) ) notifyDataSetChanged()
    }

    fun getMyBeerFragment() : BeerListFragment ? { return mainFragments[0].trueChild }
    fun getBeerDbFragment() : BeerListFragment ? { return mainFragments[1].trueChild }

    override fun getItemCount(): Int {
        return mainFragments.size
    }

    suspend fun refreshPicsOfCurrentlyVisibleBeers() {
        for( elmt in mainFragments )
        {
            val mgr = elmt.trueChild?.getLayoutMgr() ?: return
            val first = mgr.findFirstVisibleItemPosition()
            val last = mgr.findLastVisibleItemPosition()
            if( first != RecyclerView.NO_POSITION && last != RecyclerView.NO_POSITION ) {
                for( i in first .. last ) {
                    elmt.trueChild?.rcycAdapt?.sortedList?.get(i)?.beerDbData?.picUrl?.let { BeerPicCache.getPic(it) }
                }
                withContext(Dispatchers.Main){
                    elmt.trueChild?.rcycAdapt?.notifyItemRangeChanged( first, last - first )
                }
            }
        }
    }
}