/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

object NetwokMonitor {
    // Global var for the Network Monitor object
    lateinit var cm : ConnectivityManager

    private var initDone = false
    private val TAG = "NetworkMonitor"
    private var witnessedGoingDown = false
    var inetAvail = false

    private val scope = CoroutineScope( Dispatchers.IO + Job() )

    init {
        scope.launch {
            Kraken.initJob.join()

            cm.registerDefaultNetworkCallback(object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network : Network) {
                    super.onAvailable(network)
                    if(cm.getNetworkCapabilities(network)?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) == true) {
                        if(!inetAvail && witnessedGoingDown) {
                            inetAvail = true
                            scope.launch { Kraken.sectionsPagerAdapter.refreshPicsOfCurrentlyVisibleBeers() }
                        }
                        else {
                            inetAvail = true
                        }
                    }
                    else {
                        witnessedGoingDown = true
                        inetAvail = false
                    }
                }

                override fun onLost(network : Network) {
                    super.onLost(network)
                    if(cm.getNetworkCapabilities(cm.activeNetwork)?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) == true) {
                        inetAvail = true
                    }
                    else {
                        witnessedGoingDown = true
                        inetAvail = false
                    }
                }

                override fun onUnavailable() {
                    super.onUnavailable()
                    Log.i(TAG, "No net Available")
                }
            })
            initDone = true
        }
    }
}
