/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.common.hash.Hashing
import de.dullhaus.beerdb.util.Config
import de.dullhaus.beerdb.util.StyleMapper
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.withLock
import java.time.Duration
import java.time.LocalTime

object Data {
    var beerDbLive : MutableLiveData<MutableList<Beer>> = MutableLiveData()

    var modRequestsLive : MutableLiveData<MutableList<Beer>> = MutableLiveData()

    var adminMode = false

    val TAB_TITLES = arrayOf(
        R.string.tab_text_1,
        R.string.tab_text_2,
        R.string.UserModsTabText
    )

    lateinit var appName : String

    lateinit var beerStr : String

    var userHash = "-1"

    const val newerUserBeer = "Newer"

    // Global var with the BeerDB data
    private lateinit var beerDbDiskCache : BeerDiskCache

    private lateinit var sheetData : SimpleSheetsData
    // Global var which handles the user specific data
    private lateinit var userBeerDataHandler : UserBeerDataHandler

    private val scope = CoroutineScope( Dispatchers.IO + Job() )

    private var userDataSyncJob : Job ?  = null
    private val syncScope = CoroutineScope( Dispatchers.Main + Job() )

    var noPic : Drawable ? = null

    fun getAdminList() = beerDbDiskCache.getAdminList()

    var countryPref = "DE,US,GB"


//    private var lastUserRefresh : LocalTime = LocalTime.now()
    private var lastRefresh : LocalTime = LocalTime.now()
    suspend fun handleRefresh( ctxt : Context) {
//        val timeLast = if( Kraken.viewPager.currentItem == 0 ) lastUserRefresh else lastRefresh
        val timeLast = lastRefresh
        val diff = Duration.between( timeLast, LocalTime.now() )
        if( diff.seconds < 30 ) {
            Toast.makeText(ctxt, ctxt.getString( R.string.syncToast ), Toast.LENGTH_LONG).show()
            return
        }

//        if( Kraken.viewPager.currentItem == 0 ) {
//            syncUserBeers()
//            lastUserRefresh = LocalTime.now()
//        }
//        else {
            withContext(Dispatchers.IO) { sync() }
            lastRefresh = LocalTime.now()
//        }
    }

    // this saves the beerDBdata to the diskcache whenever it changes
    val syncDiskCacheObsi = Observer<MutableList<Beer>> { beerDb ->
        if( dateFromDB.isEmpty() ){
            beerDbDiskCache.saveBeerList( beerDb )
        }
        else {
            beerDbDiskCache.saveBeerList( dateFromDB,  beerDbLive.value )
            dateFromDB = ""
        }
        if( beerDbDiskCache.isSyncRequired() ) scope.launch {  syncUserBeers() }
    }

    private suspend fun syncUserBeers() {
        withContext(Dispatchers.IO) {
            userDataSyncJob?.cancel()
            userDataSyncJob = syncScope.launch {
                userBeerDataHandler.syncUserBeers( beerDbDiskCache.getUserBeerDate() )
            }
        }
    }
    private var doneOnce = false
    suspend fun init(ctxt : Context) {
        if(!doneOnce)
        {
            appName = ctxt.resources.getString( R.string.app_name )
            beerStr = ctxt.resources.getString( R.string.type_0 )
            StyleMapper.reflect( ctxt )
            doneOnce = true
        }

        withContext(Dispatchers.IO) {
            beerDbDiskCache = BeerDiskCache( ctxt, "BeerDB" )
//            userBeerDiskCache = BeerDiskCache( ctxt, "UserBeer" )
            sheetData = SimpleSheetsData(
                NetHttpTransport(),
                JacksonFactory.getDefaultInstance()
            )
            userBeerDataHandler = UserBeerDataHandler(
                NetHttpTransport(),
                JacksonFactory.getDefaultInstance()
            )
            if( beerDbDiskCache.getBeerList().isNotEmpty() ) {
                beerDbLive.postValue( beerDbDiskCache.getBeerList() )
            }

            noPic = ResourcesCompat.getDrawable( ctxt.resources, R.drawable.no_image, null )

        }

        scope.launch { sync() }
    }

    suspend fun sync() {
        // First we need to check if the user is already logged in
        // and the initjob is finished
        Kraken.authMan.loggedInMtx.withLock { Kraken.initJob.join() }

        // The hashing function decides when new users are created -> very anonymous
        userHash = Hashing.sha256().hashUnencodedChars( Kraken.authMan.signInAccount?.email  ?: "" ).toString()

        sheetData.syncBeerDbData( beerDbDiskCache.getBeerDbDate() )

        if( Config.userSheetAccess ) {
            syncUserBeers()

            checkAdmin()

            userBeerDataHandler.getAllModificationRequests()
        }
    }

    private fun checkAdmin() {

        if( beerDbDiskCache.getAdminList().find { it == Kraken.authMan.signInAccount?.email } != null ) {
            adminMode = true
            TAB_TITLES[2] = R.string.AdminTabText
        }
        else {
            adminMode = false
            // Pretty unrealistic that one gets his admin rights revoked while using the app...
            TAB_TITLES[2] = R.string.UserModsTabText
        }

    }

    fun updateThirdTab() {
        if( modRequestsLive.value == null || modRequestsLive.value?.isEmpty() == true ) {
            Kraken.sectionsPagerAdapter.removeAdminOrUserModTab()
        }
        else {
            Kraken.sectionsPagerAdapter.addAdminOrUserModTab()
        }
    }

    // TODO: Find a nice way to save the data and the date together in one go
    private var dateFromDB = ""
    fun newBeerDbData(date : String, webBeerDb : List<BeerDbData> , adminList : List<String> ) {
        val deviceBeerDb = beerDbLive.value
        dateFromDB = date
        beerDbDiskCache.setAdminList( adminList )
        if( deviceBeerDb != null && deviceBeerDb.size > 0 ){
            // First delete all elements which have been removed from the database
            val removedElements = deviceBeerDb.filter { deviceBeer -> webBeerDb.find { it.id == deviceBeer.beerDbData.id } == null }
            deviceBeerDb.removeAll( removedElements )
            // now update all elements
            for( webBeer in webBeerDb ) {
                val deviceBeer = deviceBeerDb.find { it.beerDbData.id == webBeer.id }
                if( deviceBeer == null ) {
                    deviceBeerDb.add( Beer( webBeer ) )
                }
                else {
                    deviceBeer.beerDbData = webBeer
                }
            }
            deviceBeerDb.let{ beerDbLive.postValue( it ) }
        }
        else {
            beerDbLive.postValue( webBeerDb.map { Beer( it ) }.toMutableList() )
        }
    }

    fun newUserBeerData( date : String, webUserBeers : List<UserBeer> ) {
        // First delete all elements which have been removed from the database
        val removedElements = beerDbLive.value?.filter { deviceBeer -> deviceBeer.userData != null && webUserBeers.find { it.id == deviceBeer.userData?.id } == null }
        removedElements?.forEach { it.userData = null }

        // Now merge the data into the local beerDB
        webUserBeers.forEach { webBeer -> beerDbLive.value?.find { dbBeer -> dbBeer.beerDbData.id == webBeer.id  }?.userData = webBeer }

        // Update the date of the diskcache (the actual data will be updated by the observer)
        beerDbDiskCache.updateUserDate( date )

        beerDbLive.postValue( beerDbLive.value )

    }

    fun newUserDataWrittenToSpreadsheet( date : String ){
        beerDbDiskCache.updateUserDate( date )
    }

    fun addBeerToUserBeers( beer : Beer ) {
        beer.userData = UserBeer( beer.beerDbData.id, 1 )
        updateUserBeer()
    }

    fun removeBeerFromUserBeers( beer : Beer ) {
        beer.userData = null
        updateUserBeer()
    }

    fun updateUserBeer() {
        beerDbDiskCache.userDataNeedsSync()
        // The beer has to come from the beerDbLive because this is the single source of beers
        // That is why we don't need to update or make sure it is in the list
        beerDbLive.postValue(beerDbLive.value)
    }

    fun syncModRequests( waitMillis : Long = 0 ) {
        scope.launch {
            delay( waitMillis )
            userBeerDataHandler.getAllModificationRequests()
        }
    }

    fun addModRequest( beer : Beer ) {
        userBeerDataHandler.addNewModificationRequest(beer)
        syncModRequests( 500 )
    }

    fun clearModRequest( beer : Beer ) {
        userBeerDataHandler.clearModificationRequest( beer )
        syncModRequests( 800 )
    }

    fun changeModRequest( beer : Beer) {
        // Because of different problems the safest way is to delete the old request and then add a new one
        userBeerDataHandler.clearModificationRequest( beer )
        userBeerDataHandler.addNewModificationRequest( beer )
        // TODO: Instead of delay by time we should delay until we get a feedback from the above two functions
        syncModRequests( 1200 )
    }

    fun acceptModRequest( beer : Beer ) {
        if( beer.beerDbData.id < 0 ) {
            // New beer
            sheetData.addBeer( beer )
        }
        else {
            // Change existing
            sheetData.changeBeer( beer )
        }
        clearModRequest( beer )
    }

    const val apiKey = "AIzaSyA9gUojJdrAP63WcVnM8YijVUPFRrbH2yI"
}