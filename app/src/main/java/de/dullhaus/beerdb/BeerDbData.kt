/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import de.dullhaus.beerdb.util.Utility
import java.io.Serializable
import java.time.LocalDate
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

data class BeerDbData(
    var id : Int = -1,
    var name : String = "",
    var brewery : String  = "",
    var picUrl : String = "",
    var countryCode : String = "",
    var gravity : Double = -1.0,
    var lastChanged : ZonedDateTime = ZonedDateTime.now(),
    var userHash : String = "",
    var adminData : BeerAdminData ? = null,
    var alcContent : Double = -1.0,
    var type : Int = 0,
    var brewedSince : LocalDate ? = null,
    var brewedUntil : LocalDate ? = null,
) : Serializable
{
    fun asList() : List<Any> {
        return listOf(
            id,
            name, brewery, picUrl, countryCode, Utility.doubleToStr( gravity ),
            lastChanged.format(DateTimeFormatter.ISO_ZONED_DATE_TIME),
            userHash, Utility.doubleToStr( alcContent ), type, brewedSince?.format( DateTimeFormatter.ISO_LOCAL_DATE ) ?: "",
            brewedUntil?.format( DateTimeFormatter.ISO_LOCAL_DATE ) ?: "" )
    }
    
    fun contentsTheSame( other : BeerDbData ) : Boolean {
        return name == other.name &&
                brewery == other.brewery &&
                picUrl == other.picUrl &&
                countryCode == other.countryCode &&
                gravity == other.gravity &&
                lastChanged == other.lastChanged &&
                userHash == other.userHash &&
                alcContent == other.alcContent &&
                type == other.type &&
                brewedSince == other.brewedSince &&
                brewedUntil == other.brewedUntil
    }
}
