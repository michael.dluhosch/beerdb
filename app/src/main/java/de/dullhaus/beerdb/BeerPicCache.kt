/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import android.util.LruCache
import android.webkit.URLUtil
import androidx.annotation.RequiresApi
import com.jakewharton.disklrucache.DiskLruCache
import kotlinx.coroutines.*
import java.io.File
import java.io.IOException
import java.io.OutputStream
import java.net.URL

object BeerPicCache {
    private const val TAG = "BeerPicCache"
    private const val memCacheSz = 128 * 1024 * 1024
    private val lruCache =  object : LruCache<String, Bitmap>( memCacheSz ) {
        override fun sizeOf(key: String?, value: Bitmap?): Int {
            return value?.byteCount ?: 0
        }
    }

    private lateinit var diskCache : DiskLruCache

    fun init( file : File ) {
        diskCache = DiskLruCache.open( file, 1, 1, 500 * 1024 * 1024 )
    }

    fun close() = diskCache.close()

    // unfortunately DiskLruCache does only allow specific strings as key so we need a mapping of
    // URL -> DiskLruCache keys
    private suspend fun getPicFromDisk( url: String ) : Bitmap ?  {
        return withContext(Dispatchers.IO) {
            Kraken.initJob.join()

            try {
                val snapshot = diskCache.get( url.hashCode().toString() )
                val stream = snapshot?.getInputStream(0) ?: return@withContext null
                val leBitmap = BitmapFactory.decodeStream( stream ) ?: return@withContext null
                lruCache.put( url, leBitmap )
                return@withContext leBitmap
            }
            catch(e : IllegalArgumentException ){
                Log.e( TAG, "Got illegal Arg exception $e" )
            }
            catch( e : IOException ) {
                Log.i( TAG, "Got IO Exception $e" )
            }

            return@withContext null
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    private fun writeBitmapToDisk(url : String, bitmap : Bitmap ) {
        // According to the API Documentation the compression could take several seconds
        // so we do this in the background
        GlobalScope.launch(Dispatchers.IO) {
            var editor : DiskLruCache.Editor? = null
            var stream : OutputStream? = null
            try {
                editor = diskCache.edit(url.hashCode().toString())
                if(editor != null) {
                    stream = editor.newOutputStream(0)
                    if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.R )
                        bitmap.compress(Bitmap.CompressFormat.WEBP_LOSSLESS, 75, stream)
                    else
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream)
                    editor.commit()
                }
            }
            catch(e : IOException) {
                editor?.abort()
                Log.e(TAG, "Exception during write to disk $e")
            }
            finally {
                stream?.close()
            }
        }
    }

    private suspend fun getPicFromTheInterwebs( url : String ) : Bitmap ? {
        return withContext(Dispatchers.IO) {
            // URL validity has already been checked
            try {
                if( NetwokMonitor.inetAvail ) {
                    val leStream = URL(url).openStream() ?: return@withContext null
                    val leBitmap = BitmapFactory.decodeStream(leStream) ?: return@withContext null
                    lruCache.put(url, leBitmap)
                    writeBitmapToDisk(url, leBitmap)
                    return@withContext leBitmap
                }
            }
            catch ( e : Exception)
            {
                Log.i(TAG, "Got following Exception loading picture from web: ${e.message}")
            }
            return@withContext null
        }
    }

    suspend fun getPic( url : String ) : Bitmap ? {
        if( !URLUtil.isValidUrl( url ) ) return null
        return lruCache.get( url ) ?:
            getPicFromDisk( url ) ?:
            getPicFromTheInterwebs( url )
    }

}
