/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.appbar.MaterialToolbar
import de.dullhaus.beerdb.ui.main.SectionsPagerAdapter
import kotlinx.coroutines.Job

// This object is only there because this is my first Android App and I have no clue how to do it better
object Kraken {

    // Global var to check if initialization is done
    lateinit var initJob : Job
    // Global variable to get access to the recycler views
    lateinit var sectionsPagerAdapter : SectionsPagerAdapter
    // Global var to get access to the current active tab
    lateinit var viewPager: ViewPager2
    // Global var to get access to the top menu bar
    lateinit var topAppBar : MaterialToolbar

    // Global var with authentication infos
    lateinit var authMan : AuthenticationManager

    // I feel ashamed xD
    var beerToUserify : Beer? = null
    var goToNewBeer = false
    var goToModify = false
    var goToViewPager = -1

    // I feel even more ashamed
    lateinit var main : MainActivity

}