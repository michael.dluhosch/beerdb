/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import android.util.Log
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.model.File
import com.google.api.services.sheets.v4.Sheets
import com.google.api.services.sheets.v4.model.*
import de.dullhaus.beerdb.util.Utility
import kotlinx.coroutines.*
import java.io.IOException
import java.time.Duration
import java.time.LocalTime
import java.util.*

class UserBeerDataHandler(private val transport : HttpTransport,
                          private val jsonFactory: JsonFactory
) {

    companion object {
        private val TAG = "UserBeers"
        private val scope = CoroutineScope(Dispatchers.IO + Job())
        private val folderName = "BeerTest"
        private val userBeerFileName = "UserBeerData"

        private val userModID = "1BBHW_-CqtwrQnfO4KhgrYRzlZUAHHodu7BZDgFtZfaw"
        private var spreadSheetID = ""
        private var spreadSheetModTime = ""
        private var spreadSheetRange = "A:E"

        fun userBeerListFromSheet(vals : List<List<Any>> ) : List<UserBeer> {
            return vals.map {
                UserBeer(
                    id = it.elementAt(0).toString().toInt(),
                    timesConsumed = it.elementAt(1).toString().toInt(),
                    dateAdded = Utility.convStrToZonedDateTime( it.elementAtOrElse(2, { "" } ).toString() ),
                    comment =  it.elementAtOrElse(3, { "" } ).toString(),
                    dateLastConsumed = Utility.convStrToZonedDateTime( it.elementAtOrElse(4, { "" } ).toString() ),
                )
            }
        }
    }

    private val driveApi : Drive
        get() {
            return Drive.Builder( transport,
                jsonFactory,
                Kraken.authMan.googleAccountCredential
            )
                .setApplicationName( Data.appName )
                .build()
        }

    private val sheetsAPI : Sheets
        get() {
            return Sheets.Builder( transport,
                jsonFactory,
                Kraken.authMan.googleAccountCredential
            )
                .setApplicationName( Data.appName )
                .build()
        }



    private fun writeDataToSheet() {
        clearData()
        val values = ValueRange()
        values.setValues(
            Data.beerDbLive.value?.filter{ it.userData != null }
                ?.map { it.userData?.asList() }
        )
        sheetsAPI.spreadsheets().values().update( spreadSheetID, spreadSheetRange, values ).setValueInputOption("RAW").execute()
        Log.i(TAG, "ohaa")
    }

    private fun clearData() {
        sheetsAPI.spreadsheets().values().clear( spreadSheetID, spreadSheetRange, ClearValuesRequest() ).execute()
    }


    fun addNewModificationRequest( beer : Beer ){
        scope.launch {
            // TODO: Add try/catch and find a nice way to retry
            val data = ValueRange()
            data.setValues( listOf( beer.beerDbData.asList() ) )
            val r = sheetsAPI.spreadsheets().values()
                .append(userModID, SimpleSheetsData.appendRange, data)
                .setValueInputOption("RAW")
                .setInsertDataOption("INSERT_ROWS")
                .execute()


            // This extracts the row number from a string like "Sheet1!A1:L1"
            // First get the string after the ! then split it on the : and remove all non digit numbers and then return the distinct elements
            val row = r.updates.updatedRange.split("!").last()
                .split(":").map { it.replace( "\\D+".toRegex(), "") }.distinct()
            // A modification request has to be a single row
            if( row.size != 1 ) return@launch
            val rowNr = row.first().toInt()
            sheetsAPI.spreadsheets().batchUpdate(userModID,
                BatchUpdateSpreadsheetRequest().setRequests( listOf(
                    Request().setAddProtectedRange(
                        AddProtectedRangeRequest().setProtectedRange( ProtectedRange().setRange(
                            GridRange().setStartColumnIndex(0).setEndColumnIndex( r.updates.updatedColumns )
                                .setStartRowIndex( rowNr - 1 ).setEndRowIndex( rowNr )
                        ).setDescription("ProtFor: ${beer.beerDbData.name}")
                            .setEditors( Editors().setUsers( Data.getAdminList().plus( Kraken.authMan.signInAccount?.email) ) )
                        )
                    )
                )
                )
            ).execute()
        }
    }

    fun clearModificationRequest( beer : Beer ) {
        if( beer.beerDbData.adminData  == null ) return
        scope.launch {
            // First make sure we are still talking about the same beer
            val checkRange = "${beer.beerDbData.adminData!!.row}:${beer.beerDbData.adminData!!.row}"
            val response = sheetsAPI.spreadsheets().values()
                .get( userModID, checkRange )
                .execute()
            if( response.getValues() == null ) {
                // Beer does not exist anymore
                return@launch
            }
            val curBeer = SimpleSheetsData.beerListFromSheet( response.getValues() )[0]
            if( curBeer.lastChanged.isAfter( beer.beerDbData.lastChanged ) ) {
                // Something is fishy!
                Log.w(TAG, "Time stamp changed!")
            }
            else
            {
                sheetsAPI.spreadsheets().values()
                    .clear( userModID, checkRange, ClearValuesRequest() )
                    .execute()

                // Remove protection if there is one
                val sheet = sheetsAPI.spreadsheets().get( userModID ).setIncludeGridData( false ).execute()
                val id = sheet.sheets[0].protectedRanges.find { it.range.endRowIndex == beer.beerDbData.adminData!!.row  }
                if( id?.isNotEmpty() == true ) {
                    sheetsAPI.spreadsheets().batchUpdate(userModID,
                        BatchUpdateSpreadsheetRequest().setRequests( listOf(
                            Request().setDeleteProtectedRange(
                                DeleteProtectedRangeRequest().setProtectedRangeId( id.protectedRangeId ) ) ) ) ).execute()
                }
            }
        }
    }

    fun getAllModificationRequests() {

        var response = ValueRange()
        try {
            response = sheetsAPI.spreadsheets().values()
                .get(userModID, SimpleSheetsData.range)
                .execute()
        }
        catch( ex : IOException ) {
            Log.e( TAG, "OH no ${ex.toString()}")
        }


        if( response.getValues() == null || response.values.isEmpty() ) {
            Data.modRequestsLive.postValue( mutableListOf() )
        }
        else {
            var list = SimpleSheetsData.beerListFromSheet(response.getValues()).map { Beer(it) }
            val newElmts = list.filter { it.beerDbData.id == -1 }
            // It was clear at the time of coding ... had something to do with getting unique Ids (temporary ones as the final one comes from the main spreadsheet)
            newElmts.forEachIndexed { index, beer -> beer.beerDbData.id = -index - 1 }
            if( !Data.adminMode ) {
                list = list.filter { it.beerDbData.userHash == Data.userHash }
            }
            Data.modRequestsLive.postValue( list.toMutableList() )
        }
    }

    private var lastSync : LocalTime ? = null

    suspend fun syncUserBeers( beerDate : String ) {
        withContext( Dispatchers.IO ) {
            if(lastSync == null ) {
                lastSync = LocalTime.now()
            }
            else {
                val diff = Duration.between(lastSync, LocalTime.now() )
                if( diff.seconds < 30 ) {
                    var millis = Duration.between( LocalTime.now(),  lastSync!!.plusSeconds( 30 ) ).toMillis()
                    if( millis <= 0 ) millis = 30 * 1000
                    delay( millis )
                }
            }
            lastSync = LocalTime.now()

            try {
                var metaData = checkSheetCreated( userBeerFileName )
                if( metaData.created ) {
                    spreadSheetID = metaData.id
                    spreadSheetModTime = metaData.modTime
                }
                else spreadSheetID = createSheet( userBeerFileName )

                if(spreadSheetID.isEmpty()) return@withContext

                if( beerDate == Data.newerUserBeer ) {
                    writeDataToSheet()
                    // Give google some time to update the modified timestamp
                    delay(5 * 1000 )
                    metaData = checkSheetCreated(userBeerFileName)
                    if( metaData.created ){
                        spreadSheetID = metaData.id
                        spreadSheetModTime = metaData.modTime
                    }

                    Data.newUserDataWrittenToSpreadsheet( spreadSheetModTime )
                }
                else if( spreadSheetModTime != beerDate ) {
                    // should only be the case if user modifies the data or the local data was deleted
                    val response = sheetsAPI.spreadsheets().values()
                        .get(spreadSheetID, spreadSheetRange)
                        .execute()

                    val list = userBeerListFromSheet( response.getValues() )

                    Data.newUserBeerData( spreadSheetModTime, list )
                }


            }
            catch(ex : Exception) {
                Log.w(TAG, "something is wrong here: $ex!")
            }
        }
    }

    private fun createSheet( filename : String ) : String {
        val pageToken : String ? = null
        // check if folder exists
        var result = driveApi.files().list()
            .setQ("mimeType = 'application/vnd.google-apps.folder' and name ='$folderName'")
            .setCorpora("user")
            .setSpaces("drive")
            .setFields("nextPageToken, files(id, name)")
            .setPageToken( pageToken )
            .execute()

        var folderID : String ? = null

        if( result.files.isEmpty() ) {
            val fileMetadata = File()
            fileMetadata.name = "BeerTest"
            fileMetadata.mimeType = "application/vnd.google-apps.folder"
            val file = driveApi.files().create( fileMetadata ).setFields("id").execute()
            folderID = file.id
        }
        else if( result.files.size == 1 ) {
            folderID = result.files[0].id
        }
        else {
            Log.e( TAG, "Something is fishy!" )
        }

        val elFile = File()
        elFile.name = filename
        elFile.mimeType = "application/vnd.google-apps.spreadsheet"
        elFile.parents = Collections.singletonList( folderID )
        elFile.appProperties = mapOf("BeerID" to "1337")
        val file = driveApi.files().create( elFile )
            .setFields("id")
            .execute()

        Log.i(TAG, "created file with id ${file.id}")

        return file.id
    }


    private data class SheetData( var created : Boolean, var id : String = "", var modTime : String = "" )
    private fun checkSheetCreated( filename : String ) : SheetData {
        val pageToken : String ? = null
        val result = driveApi.files().list()
            .setQ("mimeType = 'application/vnd.google-apps.spreadsheet' and name='$filename'")
            .setCorpora("user")
            .setSpaces("drive")
            .setFields("nextPageToken, files(id, name, modifiedTime)")
            .setPageToken( pageToken )
            .execute()

        for( file in result.files ) {
            Log.i(TAG, "got file: ${file.name}")
        }
        val ret = SheetData(false)
        if( result.files.size == 1 ){
            ret.created = true
            ret.id = result.files[0].id
            ret.modTime = result.files[0].modifiedTime.toStringRfc3339()
        }

        return ret
    }
}
