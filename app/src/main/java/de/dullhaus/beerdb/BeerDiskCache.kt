/*
 * Copyright 2021 Michael Dluhosch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dullhaus.beerdb

import android.content.Context
import de.dullhaus.beerdb.util.readFromFile
import de.dullhaus.beerdb.util.writeToFile
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.io.File
import java.io.Serializable

private typealias  DateStr = String

class BeerDiskCache(context: Context, filename : String ) {

    private val scope = CoroutineScope( Dispatchers.IO + Job() )

    private val TAG = "BeerData"

    private data class Data(
        var beerDbDate : DateStr,
        var userBeerDate : DateStr,
        var beerList : List<Beer>,
        var adminList : List<String>
    ) : Serializable


    private var data = Data(DateStr(), DateStr(), mutableListOf(), listOf() )

    private val file = File( context.filesDir, filename )
    init {
        val tmp : Data ? = readFromFile( file )
        if( tmp != null ) data = tmp
    }

    fun saveBeerList(list : List<Beer> ? ) {
        if( list == null ) return
        data.beerList = list
        saveToFile()
    }

    fun saveBeerList(str: DateStr, list : List<Beer> ? ) {
        if( list == null ) return
        data.beerDbDate = str
        data.beerList = list
        saveToFile()
    }

    fun setAdminList( dta : List<String> ) {
        data.adminList = dta
    }

    fun getAdminList() = data.adminList

    fun updateUserDate( str : DateStr ) {
        data.userBeerDate = str
        saveToFile()
    }

    fun userDataNeedsSync() {
        data.userBeerDate = de.dullhaus.beerdb.Data.newerUserBeer
        saveToFile()
    }

    fun isSyncRequired() : Boolean = data.userBeerDate == de.dullhaus.beerdb.Data.newerUserBeer

    private val mtx = Mutex()
    private fun saveToFile() {
        // Save data to file
        scope.launch {
            mtx.withLock {
                writeToFile( file, data )
            }
        }
    }

    fun getBeerDbDate() : DateStr { return data.beerDbDate }
    fun getUserBeerDate() : DateStr { return data.userBeerDate }
    fun getBeerList() : MutableList<Beer> { return data.beerList.toMutableList() }

}
